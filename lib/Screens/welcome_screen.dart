import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:post_jobs/Components/ask_expert_container.dart';
import 'package:post_jobs/Components/registor_button.dart';
import 'package:post_jobs/Components/theme_switch_button.dart';
import 'package:post_jobs/Provider/post_manager.dart';
import 'package:post_jobs/Provider/theme_manager.dart';
import 'package:post_jobs/Screens/main_screen.dart';
import 'package:provider/provider.dart';

class WelcomeScreen extends StatefulWidget {
  final bool switchState2;
  const WelcomeScreen({Key? key, required this.switchState2}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  bool? switchState;
  @override
  void initState() {
    super.initState();
    switchState = this.widget.switchState2;
  }

  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<ThemeManager>(context);
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: themeChanger.scaffoldBg,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Thanks for\nJoining us',
                      style: TextStyle(
                          color: themeChanger.headingsColor,
                          fontSize: 40,
                          fontWeight: FontWeight.bold),
                    ),
                    ThemeSwitchButton(
                      check: switchState!,
                      function: (val) {
                        setState(() {
                          switchState = val;
                          themeChanger.changeTheme(switchState!);
                        });
                      },
                    ),
                  ],
                ),
                SizedBox(height: 40),
                Text(
                  'What we are offering?',
                  style: TextStyle(
                      color: themeChanger.headingsColor,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10),
                AskExpertContainer(
                  bg: 'images/getYourJob.jpg',
                  buttonTitle: 'Apply',
                  title: 'Get Job',
                ),
                SizedBox(height: 20),
                AskExpertContainer(
                  bg: 'images/postJob.jpg',
                  buttonTitle: 'Post',
                  title: 'Post Job',
                ),
                SizedBox(height: 20),
                AskExpertContainer(
                  bg: 'images/doubtsInMind.jpg',
                  buttonTitle: 'Ask Expert',
                  title: 'Doubts in Mind?',
                ),
                SizedBox(height: 20),
                AskExpertContainer(
                  bg: 'images/experts.jpg',
                  buttonTitle: 'Answer Q\'s',
                  title: 'Are you Expert?',
                ),
                SizedBox(height: 20),
                AskExpertContainer(
                  bg: 'images/buy.jpg',
                  buttonTitle: 'Shop now',
                  title: 'Buy Stuff',
                ),
                SizedBox(height: 20),
                AskExpertContainer(
                  bg: 'images/sell.jpg',
                  buttonTitle: 'Sell now',
                  title: 'Sell Stuff',
                ),
                SizedBox(height: 50),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          Text(
                            'Continue into Application',
                            style: TextStyle(
                              color: themeChanger.headingsColor,
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    RegisterButton(
                      function: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return MainScreen(switchState2: switchState!);
                        }));
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
