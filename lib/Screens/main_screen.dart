import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:post_jobs/Components/about_us.dart';
import 'package:post_jobs/Components/drawer_button.dart';
import 'package:post_jobs/Components/services_text.dart';
import 'package:post_jobs/Components/theme_switch_button.dart';
import 'package:post_jobs/Models/job_posts_model.dart';
import 'package:post_jobs/Provider/post_manager.dart';
import 'package:post_jobs/Provider/theme_manager.dart';
import 'package:post_jobs/Screens/apply_job_screen.dart';
import 'package:post_jobs/Screens/get_jobs_screen.dart';
import 'package:post_jobs/Screens/home_screen.dart';
import 'package:post_jobs/Screens/question_screens.dart';
import 'package:post_jobs/Screens/shopping_screen.dart';
import 'package:post_jobs/Services/services.dart';
import 'package:provider/provider.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';

class MainScreen extends StatefulWidget {
  final bool switchState2;
  const MainScreen({Key? key, required this.switchState2}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  var _currentIndex = 0;
  bool? switchState;
  getSelectedScreen() {
    if (_currentIndex == 0) {
      return HomeScreen();
    } else if (_currentIndex == 1) {
      return ShoppingScreen();
    } else if (_currentIndex == 2) {
      return GetJobScreen();
    } else if (_currentIndex == 3) {
      return QuestionScreen();
    }
  }

  @override
  void initState() {
    super.initState();
    switchState = this.widget.switchState2;
  }

  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<ThemeManager>(context);
    final isNotPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: themeChanger.scaffoldBg,
      appBar: AppBar(
        backgroundColor: themeChanger.scaffoldBg,
        leading: Builder(
          builder: (context) => TextButton(
            onPressed: () => Scaffold.of(context).openDrawer(),
            child: Icon(
              Icons.menu,
              color: themeChanger.headingsColor,
            ),
          ),
        ),
        title: Center(
          child: Text(
            "Expert Community",
            style: TextStyle(
                color: themeChanger.headingsColor,
                fontSize: 24,
                fontWeight: FontWeight.bold),
          ),
        ),
        actions: [
          InkWell(
            onTap: () {},
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: CircleAvatar(
                backgroundColor: Colors.grey,
                backgroundImage: AssetImage("images/wadood.png"),
              ),
            ),
          ),
        ],
      ),
      drawer: Container(
        width: isNotPortrait ? width * 0.7 : width * 0.4,
        child: ClipRRect(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(15), bottomRight: Radius.circular(15)),
          child: Drawer(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    height: isNotPortrait ? height * 0.25 : height * 0.4,
                    padding: EdgeInsets.symmetric(vertical: 50, horizontal: 30),
                    decoration: BoxDecoration(
                      color: Colors.orange,
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: themeChanger.gradientColor,
                      ),
                    ),
                    child: Text(
                      "Expert Community",
                      style: TextStyle(
                        fontSize: 24.0,
                        color: themeChanger.headingsColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    color: themeChanger.draWarColor,
                    child: Column(
                      children: [
                        SizedBox(height: 30),
                        Padding(
                          padding: const EdgeInsets.only(left: 18.0),
                          child: Row(
                            children: [
                              Icon(
                                Icons.track_changes_outlined,
                                color: themeChanger.primaryColor,
                              ),
                              Text(
                                '   Change theme',
                                style: TextStyle(
                                    color: themeChanger.primaryColor,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600),
                              ),
                              SizedBox(width: 20),
                              ThemeSwitchButton(
                                size: false,
                                check: switchState!,
                                function: (val) {
                                  setState(() {
                                    switchState = val;
                                    themeChanger.changeTheme(switchState!);
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 30),
                        ServicesText(
                          color: themeChanger.primaryColor,
                        ),
                        SizedBox(height: 10),
                        DrawerButtons(
                            color: themeChanger.primaryColor,
                            icon: Icon(
                              Icons.home_outlined,
                              size: 20,
                              color: themeChanger.primaryColor,
                            ),
                            title: 'Home'),
                        DrawerButtons(
                            color: themeChanger.primaryColor,
                            icon: Icon(
                              Icons.post_add_outlined,
                              size: 20,
                              color: themeChanger.primaryColor,
                            ),
                            title: 'Post Jobs'),
                        DrawerButtons(
                            color: themeChanger.primaryColor,
                            icon: Icon(
                              Icons.wallet_travel_outlined,
                              size: 20,
                              color: themeChanger.primaryColor,
                            ),
                            title: 'Get Job'),
                        DrawerButtons(
                            color: themeChanger.primaryColor,
                            icon: Icon(Icons.shopping_cart_outlined,
                                size: 20, color: themeChanger.primaryColor),
                            title: 'Shopping'),
                        DrawerButtons(
                            color: themeChanger.primaryColor,
                            icon: Icon(
                              Icons.sell_outlined,
                              size: 20,
                              color: themeChanger.primaryColor,
                            ),
                            title: 'Sell Stuff'),
                        DrawerButtons(
                            color: themeChanger.primaryColor,
                            icon: Icon(
                              Icons.speaker_notes_outlined,
                              size: 20,
                              color: themeChanger.primaryColor,
                            ),
                            title: 'Ask Question'),
                        DrawerButtons(
                            color: themeChanger.primaryColor,
                            icon: Icon(
                              Icons.question_answer_outlined,
                              size: 20,
                              color: themeChanger.primaryColor,
                            ),
                            title: 'Answer Question'),
                        SizedBox(height: 30),
                        SizedBox(
                            height:
                                isNotPortrait ? height * 0.12 : height * 0.05),
                        AboutUs(),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: SalomonBottomBar(
        currentIndex: _currentIndex,
        onTap: (i) => setState(() => _currentIndex = i),
        items: [
          SalomonBottomBarItem(
            icon: Icon(Icons.home),
            title: Text("Home"),
            selectedColor: themeChanger.bottomBarSelectedColor,
            unselectedColor: themeChanger.bottomBarUnSelectedColor,
          ),
          SalomonBottomBarItem(
            icon: Icon(Icons.shopping_cart_outlined),
            title: Text("Shop"),
            selectedColor: themeChanger.bottomBarSelectedColor,
            unselectedColor: themeChanger.bottomBarUnSelectedColor,
          ),
          SalomonBottomBarItem(
            icon: Icon(Icons.wallet_travel_outlined),
            title: Text("Get Job"),
            selectedColor: themeChanger.bottomBarSelectedColor,
            unselectedColor: themeChanger.bottomBarUnSelectedColor,
          ),
          SalomonBottomBarItem(
            icon: Icon(Icons.question_answer),
            title: Text("Ask"),
            selectedColor: themeChanger.bottomBarSelectedColor,
            unselectedColor: themeChanger.bottomBarUnSelectedColor,
          ),
        ],
      ),
      body: getSelectedScreen(),
    );
  }
}
