import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:post_jobs/Models/job_posts_model.dart';
import 'package:post_jobs/Provider/post_manager.dart';
import 'package:post_jobs/Provider/theme_manager.dart';
import 'package:post_jobs/Services/services.dart';
import 'package:provider/provider.dart';
import 'apply_job_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool check = true;
  List<JobPostModel>? data;
  getData() async {
    final postManager = Provider.of<PostManager>(context, listen: false);
    if (postManager.post.length == 0) {
      data = await Services().getJobsPosts();
      postManager.addToList(data!);
    }
    setState(() {
      check = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    final postManager = Provider.of<PostManager>(context);
    final themeChanger = Provider.of<ThemeManager>(context);
    return Scaffold(
      backgroundColor: themeChanger.scaffoldBg,
      body: check
          ? Center(child: CircularProgressIndicator())
          : ListView.builder(
              itemCount: postManager.post.length,
              itemBuilder: (context, i) {
                return Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return ApplyJobScreen(
                            salary: postManager.post[i].salary,
                            jobTitle: postManager.post[i].jobTitle,
                            companyName: postManager.post[i].companyName,
                            email: postManager.post[i].email,
                            url:
                                'http://192.168.43.88/img/${postManager.post[i].image}',
                            phone: postManager.post[i].number,
                            time: postManager.post[i].creditHour);
                      }));
                    },
                    child: Column(
                      children: [
                        Container(
                          height: 150,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(
                                    'http://192.168.43.88/img/${postManager.post[i].image}'),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  topRight: Radius.circular(10)),
                              boxShadow: [
                                BoxShadow(
                                    offset: Offset(0, 0),
                                    color: Colors.black.withOpacity(0.3),
                                    blurRadius: 2,
                                    spreadRadius: 1)
                              ]),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 15),
                          width: double.infinity,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(10),
                                bottomRight: Radius.circular(10)),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 0),
                                color: Colors.black.withOpacity(0.3),
                                spreadRadius: 1,
                                blurRadius: 1,
                              )
                            ],
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    postManager.post[i].companyName,
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    postManager.post[i].jobTitle,
                                    style: TextStyle(color: Colors.green),
                                  ),
                                ],
                              ),
                              Text(
                                '${postManager.post[i].salary} PKR',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }),
    );
  }
}
