import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:post_jobs/Components/image_container.dart';
import 'package:post_jobs/Provider/theme_manager.dart';
import 'package:provider/provider.dart';

class ShoppingScreen extends StatefulWidget {
  const ShoppingScreen({Key? key}) : super(key: key);

  @override
  _ShoppingScreenState createState() => _ShoppingScreenState();
}

class _ShoppingScreenState extends State<ShoppingScreen> {
  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<ThemeManager>(context);
    return Scaffold(
      backgroundColor: themeChanger.scaffoldBg,
      body: Center(
        child: Container(
          padding: EdgeInsets.only(top: 30),
          child: Column(
            children: [
              Text(
                'What You want to Buy',
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: themeChanger.primaryColor),
              ),
              SizedBox(
                height: 30,
              ),
              Expanded(
                child: GridView.count(
                  primary: false,
                  padding: const EdgeInsets.all(20),
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 20,
                  crossAxisCount: 2,
                  children: [
                    GestureDetector(
                      child: ImageContainer(url: 'images/mobile.jpg'),
                      onTap: () {},
                    ),
                    GestureDetector(
                      child: ImageContainer(url: 'images/laptop.jpg'),
                      onTap: () {},
                    ),
                    GestureDetector(
                      child: ImageContainer(url: 'images/tablets.jpg'),
                      onTap: () {},
                    ),
                    GestureDetector(
                      child: ImageContainer(url: 'images/books.jpeg'),
                      onTap: () {},
                    ),
                    GestureDetector(
                      child: ImageContainer(url: 'images/cats.jpg'),
                      onTap: () {},
                    ),
                    GestureDetector(
                      child: ImageContainer(url: 'images/dogs.jpg'),
                      onTap: () {},
                    ),
                    GestureDetector(
                      child: ImageContainer(url: 'images/beds.jpg'),
                      onTap: () {},
                    ),
                    GestureDetector(
                      child: ImageContainer(url: 'images/room.jpg'),
                      onTap: () {},
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
