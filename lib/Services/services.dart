import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:post_jobs/Models/job_posts_model.dart';

class Services {
  var uriPost = Uri.parse('http://192.168.43.88/PostJob/registorUser.php');
  var uriGetJobs = Uri.parse('http://192.168.43.88/PostJob/getJobs.php');

  Future addUserToDatabase(
      String userName, String email, String password) async {
    http.Response response = await http.post(uriPost, body: {
      "name": userName,
      "email": email,
      "password": password,
    });
    print('hecking response');
    if (response.statusCode == 200) {
      print('200 doneeee');
      print(response.body);
      return response.body;
    }
  }

  Future getJobsPosts() async {
    http.Response response = await http.get(uriGetJobs);
    if (response.statusCode == 200) {
      List<JobPostModel> dataList = [];
      var data = response.body;
      var decodedData = jsonDecode(data);
      for (var item in decodedData['data']) {
        JobPostModel model = JobPostModel(
            image: item['Image'],
            email: item['Email'],
            number: item['Phone'],
            companyName: item['Company Name'],
            creditHour: item['Time'],
            jobTitle: item['Job Name'],
            salary: item['Salary']);
        dataList.add(model);
      }
      return dataList;
    }
  }
}
