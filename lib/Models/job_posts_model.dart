class JobPostModel {
  String companyName;
  String jobTitle;
  String salary;
  String creditHour;
  String email;
  String number;
  String image;
  JobPostModel(
      {required this.image,
      required this.email,
      required this.number,
      required this.companyName,
      required this.creditHour,
      required this.jobTitle,
      required this.salary});
}
