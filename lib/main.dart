import 'package:flutter/material.dart';
import 'package:post_jobs/Authentication/Sign_up.dart';
import 'package:post_jobs/Authentication/sign_in.dart';
import 'package:post_jobs/Authentication/toggle_screen.dart';
import 'package:post_jobs/Provider/post_manager.dart';
import 'package:post_jobs/Screens/apply_job_screen.dart';
import 'package:provider/provider.dart';

import 'Provider/theme_manager.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeManager>(
          create: (_) => ThemeManager(),
        ),
        ChangeNotifierProvider<PostManager>(
          create: (_) => PostManager(),
        ),
      ],
      child: Builder(
        builder: (context) {
          final themeChanger = Provider.of<ThemeManager>(context);
          return MaterialApp(
            title: 'Post Job App',
            debugShowCheckedModeBanner: false,
            themeMode: themeChanger.themeMode,
            darkTheme: ThemeData(
              scaffoldBackgroundColor: Colors.black12,
            ),
            theme: ThemeData(
              scaffoldBackgroundColor: Colors.grey,
            ),
            home: ToggleScreen(),
          );
        },
      ),
    );
  }
}
