import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:post_jobs/Components/custom_text_field.dart';
import 'package:post_jobs/Components/registor_button.dart';
import 'package:post_jobs/Components/social_media_buttons.dart';
import 'package:post_jobs/Components/terms_and_policies.dart';
import 'package:post_jobs/Components/theme_switch_button.dart';
import 'package:post_jobs/Provider/theme_manager.dart';
import 'package:post_jobs/Screens/welcome_screen.dart';
import 'package:post_jobs/Services/services.dart';
import 'package:provider/provider.dart';

class SignUp extends StatefulWidget {
  final dynamic function;
  const SignUp({Key? key, required this.function}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  bool check = true;
  bool switchState = true;
  String? userName;
  String? email;
  String? password;
  bool scroll = false;
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<ThemeManager>(context);
    final height = MediaQuery.of(context).size.height;
    return scroll
        ? Scaffold(
            body: Center(
              child: CircularProgressIndicator(
                color: Colors.deepOrangeAccent,
              ),
            ),
          )
        : Scaffold(
            body: SafeArea(
              child: Padding(
                padding: EdgeInsets.only(top: 30, left: 20, right: 20),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            onTap: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return WelcomeScreen(switchState2: switchState);
                              }));
                            },
                            child: Text(
                              'Create an\naccount',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 40,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          ThemeSwitchButton(
                            check: switchState,
                            function: (val) {
                              setState(() {
                                switchState = val;
                                themeChanger.changeTheme(switchState);
                              });
                            },
                          ),
                        ],
                      ),
                      SizedBox(height: 30),
                      Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            CustomTextField(
                              validation: (value) => value!.isEmpty
                                  ? 'Please enter your username'
                                  : null,
                              hint: 'Enter Username',
                              prefixIcon: Icons.person,
                              onChanged: (val) {
                                userName = val;
                              },
                            ),
                            CustomTextField(
                              validation: (value) => value!.isEmpty
                                  ? 'Please enter your email'
                                  : null,
                              hint: 'Enter Email',
                              prefixIcon: Icons.email,
                              onChanged: (val) {
                                email = val;
                              },
                            ),
                            CustomTextField(
                              validation: (value) => value!.isEmpty
                                  ? 'Please enter your password'
                                  : null,
                              hint: 'Enter Password',
                              prefixIcon: Icons.lock_sharp,
                              suffixIcon: check
                                  ? Icons.visibility_off_outlined
                                  : Icons.visibility_outlined,
                              check: check,
                              function: () {
                                setState(() {
                                  check = !check;
                                });
                              },
                              onChanged: (val) {
                                password = val;
                              },
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                      TermsAndPoliciesText(),
                      SizedBox(height: 40),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Register',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          RegisterButton(function: () async {
                            if (_formKey.currentState!.validate()) {
                              setState(() {
                                scroll = true;
                              });
                              final result = await Services().addUserToDatabase(
                                  userName!, email!, password!);
                              print(result);
                              if (result == "success") {
                                Navigator.pushReplacement(context,
                                    MaterialPageRoute(builder: (context) {
                                  return WelcomeScreen(
                                    switchState2: switchState,
                                  );
                                }));
                              }
                              setState(() {
                                scroll = false;
                              });
                            }
                          }),
                        ],
                      ),
                      SizedBox(
                        height: height * 0.09,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'sign up with',
                            style: TextStyle(
                              color: Colors.white.withOpacity(0.6),
                            ),
                          ),
                          SizedBox(height: 20),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 30),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                SocialMediaButton(icon: FontAwesome.google),
                                SocialMediaButton(icon: FontAwesome.apple),
                                SocialMediaButton(icon: FontAwesome.facebook),
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: height * 0.068,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Already have account? ',
                            style: TextStyle(
                              color: Colors.white.withOpacity(0.6),
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          InkWell(
                            onTap: this.widget.function,
                            child: Text(
                              'Sign In',
                              style: TextStyle(
                                color: Colors.deepOrangeAccent,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
  }
}
