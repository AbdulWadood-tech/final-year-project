import 'package:flutter/cupertino.dart';
import 'package:post_jobs/Authentication/Sign_up.dart';
import 'package:post_jobs/Authentication/sign_in.dart';

class ToggleScreen extends StatefulWidget {
  const ToggleScreen({Key? key}) : super(key: key);

  @override
  _ToggleScreenState createState() => _ToggleScreenState();
}

class _ToggleScreenState extends State<ToggleScreen> {
  bool check = false;
  toggleIt() {
    setState(() {
      check = !check;
    });
  }

  @override
  Widget build(BuildContext context) {
    return check
        ? SignIn(
            function: () {
              toggleIt();
            },
          )
        : SignUp(
            function: () {
              toggleIt();
            },
          );
  }
}
