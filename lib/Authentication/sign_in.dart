import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:post_jobs/Components/custom_text_field.dart';
import 'package:post_jobs/Components/registor_button.dart';
import 'package:post_jobs/Components/social_media_buttons.dart';
import 'package:post_jobs/Components/theme_switch_button.dart';
import 'package:post_jobs/Provider/theme_manager.dart';
import 'package:post_jobs/Screens/welcome_screen.dart';
import 'package:provider/provider.dart';

class SignIn extends StatefulWidget {
  final dynamic function;
  const SignIn({Key? key, required this.function}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  bool check = true;
  bool switchState = true;
  String? email;
  String? password;
  bool scroll = false;
  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<ThemeManager>(context);
    final height = MediaQuery.of(context).size.height;
    return scroll
        ? Scaffold(
            body: Center(
              child: CircularProgressIndicator(
                color: Colors.deepOrangeAccent,
              ),
            ),
          )
        : Scaffold(
            body: SafeArea(
              child: Padding(
                padding: EdgeInsets.only(top: 30, left: 20, right: 20),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            onTap: () {
                              Navigator.pushReplacement(context,
                                  MaterialPageRoute(builder: (context) {
                                return WelcomeScreen(switchState2: switchState);
                              }));
                            },
                            child: Text(
                              'Sign in to\naccount',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 40,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          ThemeSwitchButton(
                            check: switchState,
                            function: (val) {
                              setState(() {
                                switchState = val;
                                themeChanger.changeTheme(switchState);
                              });
                            },
                          ),
                        ],
                      ),
                      SizedBox(height: 30),
                      CustomTextField(
                        hint: 'Enter Email',
                        prefixIcon: Icons.email,
                        onChanged: (val) {
                          email = val;
                        },
                      ),
                      CustomTextField(
                        hint: 'Enter Password',
                        prefixIcon: Icons.lock_sharp,
                        suffixIcon: check
                            ? Icons.visibility_off_outlined
                            : Icons.visibility_outlined,
                        check: check,
                        function: () {
                          setState(() {
                            check = !check;
                          });
                        },
                        onChanged: (val) {
                          password = val;
                        },
                      ),
                      SizedBox(height: 40),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Sign In',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          RegisterButton(
                            function: () {},
                          ),
                        ],
                      ),
                      SizedBox(
                        height: height * 0.2,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'sign in with',
                            style: TextStyle(
                              color: Colors.white.withOpacity(0.6),
                            ),
                          ),
                          SizedBox(height: 20),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 30),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                SocialMediaButton(icon: FontAwesome.google),
                                SocialMediaButton(icon: FontAwesome.apple),
                                SocialMediaButton(icon: FontAwesome.facebook),
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: height * 0.11,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Don\'t have account? ',
                            style: TextStyle(
                              color: Colors.white.withOpacity(0.6),
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          InkWell(
                            onTap: this.widget.function,
                            child: Text(
                              'Sign Up',
                              style: TextStyle(
                                color: Colors.deepOrangeAccent,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
  }
}
