import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AskExpertContainer extends StatelessWidget {
  final String bg;
  final String title;
  final String buttonTitle;
  const AskExpertContainer({
    Key? key,
    required this.title,
    required this.bg,
    required this.buttonTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      alignment: Alignment.center,
      height: height * 0.8,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 0),
            color: Colors.black,
            blurRadius: 5,
            spreadRadius: 0.1,
          )
        ],
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(
          image: AssetImage(bg),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: TextStyle(
                color: Colors.white,
                fontSize: 25,
                fontWeight: FontWeight.bold,
                shadows: [
                  Shadow(
                    offset: Offset(0, 0),
                    color: Colors.black.withOpacity(0.1),
                    blurRadius: 5,
                  )
                ]),
          ),
          Container(
            width: width * 0.4,
            padding: EdgeInsets.symmetric(vertical: 15),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Text(
              buttonTitle,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            ),
          ),
        ],
      ),
    );
  }
}
