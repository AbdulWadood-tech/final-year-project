import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TermsAndPoliciesText extends StatelessWidget {
  const TermsAndPoliciesText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
              text: 'By clicking the ',
              style: TextStyle(
                color: Colors.white.withOpacity(0.6),
                fontWeight: FontWeight.w300,
              )),
          TextSpan(
            text: 'Register ',
            style: TextStyle(
              color: Colors.deepOrangeAccent,
              fontWeight: FontWeight.w300,
            ),
          ),
          TextSpan(
            text: 'button, you agreed to\nour terms and policies',
            style: TextStyle(
              color: Colors.white.withOpacity(0.6),
              fontWeight: FontWeight.w300,
            ),
          ),
        ],
      ),
    );
  }
}
