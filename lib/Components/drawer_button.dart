import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DrawerButtons extends StatelessWidget {
  final String title;
  final Icon icon;
  final Color color;
  const DrawerButtons({
    Key? key,
    required this.color,
    required this.title,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Padding(
        padding: const EdgeInsets.only(left: 45.0, top: 20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            icon,
            Padding(
              padding: EdgeInsets.only(left: 8.0),
              child: Text(
                title,
                style: TextStyle(
                    fontSize: 12.5, fontWeight: FontWeight.w600, color: color),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
