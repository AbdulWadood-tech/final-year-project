import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ServicesText extends StatelessWidget {
  final Color color;
  const ServicesText({
    Key? key,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 18.0),
      child: Row(
        children: [
          Icon(
            Icons.list,
            color: color,
          ),
          Text(
            "   Services",
            style: TextStyle(
                color: color, fontSize: 16, fontWeight: FontWeight.w600),
          ),
        ],
      ),
    );
  }
}
