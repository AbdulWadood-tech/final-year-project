import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:post_jobs/Provider/theme_manager.dart';
import 'package:provider/provider.dart';

class CustomTextField extends StatelessWidget {
  final String hint;
  final IconData prefixIcon;
  final IconData? suffixIcon;
  final dynamic function;
  final bool check;
  final dynamic onChanged;
  final dynamic validation;
  const CustomTextField({
    Key? key,
    this.validation,
    required this.onChanged,
    this.check = false,
    this.function,
    required this.hint,
    required this.prefixIcon,
    this.suffixIcon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeChanger = Provider.of<ThemeManager>(context);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 13),
      child: TextFormField(
        validator: validation,
        onChanged: onChanged,
        obscureText: check ? true : false,
        style: TextStyle(color: Colors.white.withOpacity(0.8)),
        decoration: InputDecoration(
          fillColor: themeChanger.color,
          filled: true,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: themeChanger.borderColor, width: 0.0),
            borderRadius: BorderRadius.circular(10),
          ),
          prefixIcon: Icon(
            prefixIcon,
            color: themeChanger.textColor,
            size: 20,
          ),
          suffixIcon: IconButton(
              onPressed: function,
              icon: Icon(
                suffixIcon,
                color: check ? themeChanger.textColor : Colors.blue,
              )),
          hintText: hint,
          hintStyle: TextStyle(
            color: themeChanger.textColor,
          ),
        ),
      ),
    );
  }
}
