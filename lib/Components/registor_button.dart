import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegisterButton extends StatelessWidget {
  final dynamic function;
  const RegisterButton({
    Key? key,
    required this.function,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(4),
      decoration: BoxDecoration(
          color: Colors.deepOrangeAccent,
          borderRadius: BorderRadius.circular(30),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 20),
                color: Colors.deepOrangeAccent.withOpacity(0.7),
                blurRadius: 45,
                spreadRadius: 1)
          ]),
      child: IconButton(
          onPressed: function,
          icon: Icon(
            Icons.arrow_forward,
            color: Colors.white,
            size: 30,
          )),
    );
  }
}
