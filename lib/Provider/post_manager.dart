import 'package:flutter/cupertino.dart';
import 'package:post_jobs/Models/job_posts_model.dart';

class PostManager extends ChangeNotifier {
  List<JobPostModel> post = [];

  addToList(List<JobPostModel> list) {
    post = list;
    notifyListeners();
  }
}
