import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ThemeManager extends ChangeNotifier {
  var themeMode = ThemeMode.light;
  Color color = Colors.black.withOpacity(0.2);
  Color textColor = Colors.white;
  Color borderColor = Colors.grey;
  Color bottomBarSelectedColor = Colors.blue;
  Color bottomBarUnSelectedColor = Colors.black;
  Color scaffoldBg = Colors.white;
  Color headingsColor = Colors.black;
  Color primaryColor = Colors.black;
  List<Color> gradientColor = [
    Colors.deepOrange.withOpacity(0.9),
    Colors.orange.withOpacity(0.6),
  ];
  Color draWarColor = Colors.white;

  // Colors.black.withOpacity(0.7)

  void changeTheme(bool check) {
    if (check == false) {
      themeMode = ThemeMode.dark;
      color = Color(0xff262626);
      textColor = Colors.grey.withOpacity(0.5);
      borderColor = Colors.black;
      bottomBarSelectedColor = Colors.white;
      bottomBarUnSelectedColor = Colors.grey;
      headingsColor = Colors.white;
      scaffoldBg = Colors.black12;
      primaryColor = Colors.white;
      gradientColor = [
        Colors.black,
        Colors.black.withOpacity(0.8),
      ];
      draWarColor = Colors.black.withOpacity(0.8);
      notifyListeners();
    } else {
      themeMode = ThemeMode.light;
      color = Colors.black.withOpacity(0.2);
      borderColor = Colors.grey;
      textColor = Colors.white;
      bottomBarSelectedColor = Colors.blue;
      bottomBarUnSelectedColor = Colors.black;
      scaffoldBg = Colors.white;
      headingsColor = Colors.black;
      primaryColor = Colors.black;
      gradientColor = [
        Colors.deepOrange.withOpacity(0.9),
        Colors.orange.withOpacity(0.6),
      ];
      draWarColor = Colors.white;
      notifyListeners();
    }
  }
}
